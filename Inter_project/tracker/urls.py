from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from tracker import views

urlpatterns = [
    path('tracker/', views.tracker_get_trackers),
    path('tracker/<int:pk>', views.tracker_get_tracker),
    path('tracker/force/<int:pk>', views.tracker_put_force_expire),
    path('tracker/reset/<int:pk>', views.tracker_put_reset_expire),
]

