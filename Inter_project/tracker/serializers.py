from rest_framework import serializers
from tracker.models import Tracker, return_date_time
try:
    from django.contrib.auth import get_user_model
except ImportError: # django < 1.5
    from django.contrib.auth.models import User
else:
    User = get_user_model()

class TrackerSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    user = serializers.PrimaryKeyRelatedField(read_only=True)
    creation_date = serializers.DateTimeField()
    expire_date = serializers.DateTimeField()
    allowed = serializers.BooleanField()

    def create(self, validated_data):
        """
        Create and return a new `Tracker` instance, given the validated data.
        """
        return Tracker.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Tracker` instance, given the validated data.
        """
        instance.user = validated_data.get('user', instance.user)
        instance.creation_date = validated_data.get('creation_date', instance.creation_date)
        instance.expire_date = validated_data.get('expire_date', instance.expire_date)
        instance.allowed = validated_data.get('allowed',instance.allowed)
        instance.save()
        return instance
# Create your views here.

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'tracker')
        read_only_fields = ('id', 'tracker')
