from django.contrib.auth import logout
from django.utils.deprecation import MiddlewareMixin

class ForceLogoutMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if request.user.is_authenticated  and \
           ( 'LAST_REQUEST_DATE' not in request.session or \
             request.session['LAST_REQUEST_DATE'] < request.user.tracker.expire_date ):
            request.user.tracker.force_expire()
            logout(request)