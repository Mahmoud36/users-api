from datetime import datetime, timedelta
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.signals import user_logged_in
from django.core.signals import request_started
from django.db.models import signals
from django.contrib.auth import login as dj_login
from django.dispatch import receiver
from django.contrib.auth.models import User





"""
    * Add connection to the users table and add two field.
    - user represents the user id which will be expired
    - force_expire_date represents the expire date that will be compared
        against
    * user may not be required if everything will be in the same table
    * find a way to force the jwt authentication to expire
"""

def return_future_date_time():
    now = datetime.now()
    return now + timedelta(days=3650)

def return_date_time():
    now = datetime.now()
    return now 

class Tracker(models.Model):
    # probably not needed 
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name='user'
    )
    creation_date = models.DateTimeField(default=return_date_time)
    expire_date = models.DateTimeField(default=return_future_date_time)
    allowed = models.BooleanField(default=True)

    def force_expire(self):
        self.expire_date = datetime.now()
        self.allowed = False
        self.save()

    def reset_expire(self, date):
        self.expire_date = datetime.now()+timedelta(years=10)
        self.allowed = True
        self.save()

    def __str__(self):
        return '{}'.format(self.user.id)
"""
def update_session_last_request(sender, user, request, **kwargs):
    if request:
        request.session['LAST_REQUEST_DATE'] = datetime.now()
request_started.connect(update_session_last_request)"""

@receiver(request_started, sender=User)
def update_session_last_request(sender, request, **kwargs):
    if request:
        request.session['LAST_REQUEST_DATE'] = datetime.now()

def create_tracker(sender, instance, created, **kwargs):
    """Create ModelB for every new ModelA."""
    if created:
        Tracker.objects.create(thing=instance)

signals.post_save.connect(create_tracker, sender=User, weak=False,
                          dispatch_uid='models.create_tracker')