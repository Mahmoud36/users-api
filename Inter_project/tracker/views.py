from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from tracker.models import Tracker
from tracker.serializers import TrackerSerializer,UserSerializer
from django.contrib.auth import logout

try:
    from django.contrib.auth import get_user_model
except ImportError: # django < 1.5
    from django.contrib.auth.models import User
else:
    User = get_user_model()


@api_view(['GET',])
def tracker_get_trackers(request):
    trackers = Tracker.objects.all()
    tracker = TrackerSerializer(trackers, many=True)
    return Response(tracker.data)

@api_view(['GET',])
def tracker_get_tracker(request,pk):
    try:
        user = User.objects.get(pk=pk)
        tracker = Tracker.objects.get(pk=user)
    except Tracker.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'GET':
        serializer = TrackerSerializer(tracker)
        return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT',])
def tracker_put_force_expire(request, pk):
    try:
        user = User.objects.get(pk=pk)
        tracker = Tracker.objects.get(pk=user)
    except Tracker.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'PUT':
        tracker.force_expire()
        content = "Tracker expired succesfully"
        type = "message" 
        return Response(data={"content":content,"type":type})

@api_view(['PUT',])
def tracker_put_reset_expire(request, pk):
    try:
        user = User.objects.get(pk=pk)
        tracker = Tracker.objects.get(pk=user)
    except Tracker.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'PUT':
        tracker.reset_expire()
        content = "Tracker reset succesfully"
        type = "message" 
        return Response(data={"content":content,"type":type})